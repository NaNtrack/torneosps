<?php
App::uses('AppController', 'Controller');
App::uses('XML', 'Utility');

/**
 * Tourneys Controller
 *
 * @property Tourney $Tourney
 */
class TourneysController extends AppController {

	private $_sources = array(
		'pokerstars' => array(
			'http://www.pokerstars.com/datafeed_global/tournaments/regular.xml?&sid='
		)
	);

	/**
	 * Retrieve the latest poker tournament information
	 *
	 * @param string $source The poker room source
	 */
	public function retrieve ($source = 'pokerstars') {
		foreach ($this->_sources[$source] as $xmlURL) {
			$xmlObj = XML::build($xmlURL.rand(1,100)/10000);
			$data = XML::toArray($xmlObj);
			$processed = 0;
			$new = 0;
			$updated = 0;

			$this->Tourney->updateAll(array('status' => "'Closed'"));

			foreach ($data['selected_tournaments']['tournament'] as $tournament) {
				$processed += 1;
				$t = $this->Tourney->findByTourneyId($tournament['@id']);
				$tourney = array(
					'Tourney' => array(
						'id'               => $t ? $t['Tourney']['id'] : NULL,
						'tourney_id'       => $tournament['@id'],
						'guaranteed'       => isset($tournament['@guaranteed']) ? 1 : 0,
						'rebuy'            => isset($tournament['@rebuy']) ? 1 : 0,
						'addon'            => isset($tournament['@addon']) ? 1 : 0,
						'turbo'            => isset($tournament['@turbo']) ? 1 : 0,
						'play_money'       => isset($tournament['@play_money']) ? 1 : 0,
						'status'           => $tournament['@status'],
						'players'          => $tournament['@players'],
						'award_places'     => $tournament['@award_places'],
						'prize'            => $tournament['@prize'],
						'parent_id'        => isset($tournament['@parent_id']) ? $tournament['@parent_id'] : NULL,
						'name'             => $tournament['name'],
						'description'      => $tournament['description'],
						'start_date'       => date('Y-m-d H:i:s', strtotime($tournament['start_date'])),
						'game'             => $tournament['game'],
						'buy_in_fee'       => $tournament['buy_in_fee'],
						'buy_in_amount'    => is_array($tournament['buy_in_amount']) ? $tournament['buy_in_amount']['@'] : $tournament['buy_in_amount'],
						'buy_in_currency'  => is_array($tournament['buy_in_amount']) ? $tournament['buy_in_amount']['@currency'] : 'USD',
						'fpp_fee'          => $tournament['fpp_fee'],
						'fee_amount'       => is_array($tournament['fee_amount']) ? $tournament['fee_amount']['@'] : $tournament['fee_amount'],
						'fee_currency'     => is_array($tournament['fee_amount']) ? $tournament['fee_amount']['@currency'] : 'USD',
						'limit'            => $tournament['limit'],
						'table_players'    => $tournament['max_table_players'],
						'chips'            => $tournament['chips']
					)
				);
				$this->Tourney->create();
				$saved = $this->Tourney->save($tourney);
				if (!$saved) {
					CakeLog::debug(print_r($this->Tourney->invalidFields(), true));
				} else {
					$new += $t ? 0 : 1;
					$updated += $t ? 1 : 0;
				}
			}
			$this->set(compact('new', 'updated', 'processed'));
		}
	}

}
