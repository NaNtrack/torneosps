<?php
App::uses('AppModel', 'Model');

/**
 * Tourney Model
 *
 * @property Tourney $Tourney
 */
class Tourney extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';

		public $validate = array(
		'tourney_id'    => array('numeric'  => array('rule' => array('numeric')),'notempty' => array('rule' => array('notempty'))),
		'guaranteed'    => array('boolean'  => array('rule' => array('boolean'))),
		'rebuy'         => array('boolean'  => array('rule' => array('boolean'))),
		'addon'         => array('boolean'  => array('rule' => array('boolean'))),
		'turbo'         => array('boolean'  => array('rule' => array('boolean'))),
		'play_money'    => array('boolean'  => array('rule' => array('boolean'))),
		'status'        => array('notempty' => array('rule' => array('notempty'))),
		'players'       => array('numeric'  => array('rule' => array('numeric'))),
		'award_places'  => array('numeric'  => array('rule' => array('numeric'))),
		'parent_id'     => array('numeric'  => array('rule' => array('numeric'), 'allowEmpty' => true)),
		'name'          => array('notempty' => array('rule' => array('notempty'))),
		'description'   => array('notempty' => array('rule' => array('notempty'))),
		'start_date'    => array('datetime' => array('rule' => array('datetime'))),
		'buy_in_fee'    => array('notempty' => array('rule' => array('notempty'))),
		'buy_in_amount' => array('numeric'  => array('rule' => array('numeric'))),
		'fpp_fee'       => array('notempty' => array('rule' => array('notempty'))),
		'fpp_amount'    => array('numeric'  => array('rule' => array('numeric'))),
		'limit'         => array('notempty' => array('rule' => array('notempty'))),
		'table_players' => array('numeric'  => array('rule' => array('numeric'))),
		'chips'         => array('numeric'  => array('rule' => array('numeric')))
	);


	public function beforeSave($options = array()) {
		$this->validateStatus();
		$this->validateGame();
		$this->parsePrice();
		return true;
	}


	private function validateStatus () {
		$date = $this->data['Tourney']['start_date'];
		$_20_minutes_ago = date('Y-m-d H:i:s', strtotime('-20 minutes'));
		if ($date < $_20_minutes_ago) {
			$this->data['Tourney']['status'] = 'Closed';
		} elseif ($date > $_20_minutes_ago && $date < date('Y-m-d H:i:s')) {
			$this->data['Tourney']['status'] = 'Late registration';
		}
	}

	private function validateGame () {
		if ($this->data['Tourney']['game'] == '') {
			if (strpos($this->data['Tourney']['name'], '5-Card Omaha') > 0) {
				$this->data['Tourney']['game'] = '5-Card Omaha';
			} elseif (strpos($this->data['Tourney']['name'], 'Courchevel') > 0) {
				$this->data['Tourney']['game'] = 'Courchevel';
			} else {
				$this->data['Tourney']['game'] = 'Other';
			}
		}
	}

	private function parsePrice () {
		$prize = $this->data['Tourney']['prize'];
		$prize = substr($prize, 1);
		$prize = str_replace(',', '', $prize);
		$this->data['Tourney']['prize'] = $prize;
	}

}
